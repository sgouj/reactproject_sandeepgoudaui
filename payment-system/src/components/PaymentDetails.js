import React from "react";
import DetailedAccordion from "./DetailedAccordion";

const PaymentDetails = (props) => {
	return (
		<React.Fragment>
			<div style={{ paddingLeft: "131px" }}>
				<DetailedAccordion payment={props.payment} />
			</div>
		</React.Fragment>
	);
};

export default PaymentDetails;
