import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchPaymentApiStatus } from "../actions/index";

const About = () => {
	const dispatch = useDispatch();
	const apiStatus = useSelector((state) => state.apiStatus);
	useEffect(() => {
		dispatch(fetchPaymentApiStatus());
	}, []);
	return <h4>{apiStatus}</h4>;
};

export default About;
