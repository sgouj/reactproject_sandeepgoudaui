import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addPaymentDetails } from "../actions/index";

const AddPayment = () => {
	const dispatch = useDispatch();
	const isSuccessfullySubmitted = useSelector((state) => state.isPaymentSaved);
	const isSubmitFailed = useSelector((state) => state.isPaymentError);

	const { register, handleSubmit, errors, formState } = useForm({
		mode: "onBlur",
	});
	const handlePaymentSybmit = async (data, event) => {
		event.preventDefault();
		console.log(data);
		await dispatch(addPaymentDetails(data));
		console.log("form details saved successfully");
	};

	const validatePaymentDate = (paymentDate) => {
		if (paymentDate) {
			if (
				paymentDate.match(
					/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/
				)
			) {
				console.log("regex matched");
				return true;
			} else {
				console.log("regx not matched");
				return false;
			}
		} else {
			console.log("empty");
			return false;
		}
	};

	return (
		<div className="row">
			<div className="col-lg-12">
				<h1>Add New Payment</h1>
				<br></br>
				{isSuccessfullySubmitted && (
					<div className="alert alert-success fade show">
						<div className="alert-heading">
							<i className="fa fa-success"></i> Payment Details submitted
							successfully
						</div>
					</div>
				)}
				{isSubmitFailed && (
					<div className="alert alert-warning fade show">
						<div className="alert-heading">
							<i className="fa fa-warning"></i> Payment Details could not be
							saved now.. Please try again later.
						</div>
					</div>
				)}
				<br></br>
				<form onSubmit={handleSubmit(handlePaymentSybmit)}>
					<div className="form-row">
						<div className="form-group col-md-3">
							<label htmlFor="inputPaymentId">Payment Id : </label>
							<input
								ref={register({ required: true, maxLength: 3 })}
								name="id"
								type="number"
								className="form-control"
								id="inputPaymentId"
								style={{ borderColor: errors.id && "red" }}
								placeholder="enter payment id"
							/>
							<p style={{ color: "red" }}>
								{errors.id &&
									errors.id.type === "required" &&
									"Payment Id is required"}
								{errors.id &&
									errors.id.type === "maxLength" &&
									"Payment Id is cannot be more than 1000"}
							</p>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="paymentCustId">Customer Id : </label>
							<input
								ref={register({ required: true, maxLength: 3 })}
								name="custId"
								type="number"
								className="form-control"
								id="paymentCustId"
								style={{ borderColor: errors.custId && "red" }}
								placeholder="enter customer id"
							/>
							<p style={{ color: "red" }}>
								{errors.custId &&
									errors.custId.type === "required" &&
									"Payment customer id is required"}
								{errors.custId &&
									errors.custId.type === "maxLength" &&
									"Payment customer Id is cannot be more than 1000"}
							</p>
						</div>
					</div>
					<div className="form-row">
						<div className="form-group col-md-2">
							<label htmlFor="paymentType">Payment Type :</label>
							<select
								name="type"
								ref={register({ required: true })}
								id="paymentType"
								className="form-control"
								style={{ borderColor: errors.type && "red" }}
								placeholder="enter payment type"
							>
								<option value="">Choose...</option>
								<option value="debit card">Debit Card</option>
								<option value="credit card">Credit Card</option>
							</select>
							<p style={{ color: "red" }}>
								{errors.type && "Payment type is required"}
							</p>
						</div>
						<div className="form-group col-md-2">
							<label htmlFor="paymentdate">Payment Date :</label>
							<input
								ref={register({
									required: true,
									validate: (input) => validatePaymentDate,
								})}
								name="paymentDate"
								type="date"
								className="form-control"
								id="paymentDate"
								style={{
									borderColor: errors.paymentdate && "red",
								}}
							/>
							<p style={{ color: "red" }}>
								{errors.paymentDate &&
									errors.paymentDate.type === "required" &&
									"Payment Date is Required"}
								{errors.paymentDate &&
									errors.paymentDate.type === "validate" &&
									"Enter a valid payment date"}
							</p>
						</div>
					</div>
					<button
						type="submit"
						className="btn btn-primary"
						disabled={formState.isSubmitting}
					>
						Submit
					</button>
				</form>
			</div>
		</div>
	);
};

export default AddPayment;
