import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { fetchPaymentsByType, fetchPaymentsById } from "../actions";
import { Form, ButtonGroup, ToggleButton } from "react-bootstrap";
import DisplayPayments from "./DisplayPayments";
import AddPayment from "./AddPayment";

const Search = (props) => {
	const { register, handleSubmit, errors } = useForm();
	const [radioButtonValue, setRadioButtonValue] = useState("By Id");
	const dispatch = useDispatch();
	const { payments, isPaymentsTypeCalled } = useSelector((state) => state);

	const onSubmit = (data) => {
		console.log("submitted Data", data);
		if (radioButtonValue === "By Type") {
			dispatch(fetchPaymentsByType(data));
		} else {
			dispatch(fetchPaymentsById(data));
		}
	};

	const radios = [
		{ name: "By Id", value: "By Id", id: "1" },
		{ name: "By Type", value: "By Type", id: "2" },
	];

	const handleRadioButton = (e) => {
		setRadioButtonValue(e.currentTarget.value);
	};

	return (
		<React.Fragment>
			<div className="container" style={{ paddingTop: "18px" }}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className="navbar-form navbar-left form-inline"
				>
					<div className="form-group">
						<input
							className="form-control mr-sm-2"
							name="search"
							ref={register}
						/>
					</div>
					<input
						type="submit"
						value="SEARCH"
						className="btn btn-outline-success my-2 my-sm-0"
					/>
				</form>
				{radios.map((radio, idx) => {
					return (
						<div className="custom-control custom-radio custom-control-inline">
							<input
								className="form-check-input custom-control-input"
								key={idx}
								type="radio"
								name="serachById"
								value={radio.value}
								checked={radioButtonValue === radio.value}
								onChange={(e) => handleRadioButton(e)}
							/>
							<label className="custom-control-label">{radio.name}</label>
						</div>
					);
				})}
			</div>
			{payments !== undefined && payments !== null && payments.length > 0 ? (
				<DisplayPayments />
			) : (
				` No Records for this search`
			)}
		</React.Fragment>
	);
};

export default Search;
