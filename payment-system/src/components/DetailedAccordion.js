import React from "react";
import DownArrow from "./images/arrowdown.svg";
import UpArrow from "./images/uparrow.svg";

const DetailedAccordion = (props) => {
	const [expanded, setExpanded] = React.useState(false);
	const [panelId, setPanelId] = React.useState("");

	const handleChange = (e, panel) => {
		setPanelId(panel);
		setExpanded(!expanded);
		// setExpanded(!expanded);
	};

	return (
		<React.Fragment>
			<div className="accordion" id="accordionExample">
				<div className="card">
					<div
						className="card-header"
						style={{ display: "flex" }}
						key={props.payment.id}
						id="headingOne"
						onClick={(e) => handleChange(e, props.payment.id)}
					>
						<h5 className="mb-0">
							<span>{props.payment.id}</span>
						</h5>
						{expanded ? (
							<img
								src={UpArrow}
								style={{ marginLeft: "auto", width: "20px" }}
								className="MuiSvgIcon-root"
							/>
						) : (
							<img
								src={DownArrow}
								style={{ marginLeft: "auto", width: "20px" }}
								className="MuiSvgIcon-root"
							/>
						)}
					</div>

					{panelId === props.payment.id && expanded && (
						<div
							id="collapseOne"
							className="collapse show"
							aria-labelledby="headingOne"
							data-parent="#accordionExample"
						>
							<div className="card-body">
								<h6>ID : {props.payment.id}</h6>
								<h6>Type : {props.payment.type}</h6>
								<h6>Date : {props.payment.paymentDate}</h6>
							</div>
						</div>
					)}
				</div>
			</div>
		</React.Fragment>
	);
};

export default DetailedAccordion;
