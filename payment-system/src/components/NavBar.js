import React from "react";
import { Link, Route } from "react-router-dom";

const NavBar = (props) => {
	return (
		<React.Fragment>
			<nav className="navbar navbar-expand-lg navbar-dark bg-primary">
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<div>
						<span className="navbar-nav mr-auto">Allstate</span>
					</div>
					<ul className="navbar-nav mr-auto">
						<Link className="nav-link" to="/">
							<li className="nav-item">About</li>
						</Link>
						<Link className="nav-link" to="/Search">
							<li className="nav-item">Search</li>
						</Link>
						<Link className="nav-link" to="/AddPayment">
							<li className="nav-item">Add Payment</li>
						</Link>
					</ul>
				</div>
			</nav>
		</React.Fragment>
	);
};

export default NavBar;
