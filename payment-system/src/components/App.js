import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { fetchPaymentsByType } from "../actions";
import NavBar from "./NavBar";
import About from "./About";
import Search from "./Search";
import AddPayment from "./AddPayment";

function App() {
	const dispatch = useDispatch();
	// const payments = useSelector((state) => state.payments);
	// console.log(payments);
	// useEffect(() => {
	// 	dispatch(fetchPaymentsByType());
	// }, []);
	return (
		<Router>
			<div>
				<NavBar />
				{/* <Switch>
					<Route path="/" component={NavBar}></Route>
				</Switch> */}
				<Route exact path="/">
					<About />
				</Route>
				<Route path="/Search">
					<Search />
				</Route>
				<Route path="/AddPayment">
					<AddPayment />
				</Route>
			</div>
		</Router>
	);
}

export default App;
