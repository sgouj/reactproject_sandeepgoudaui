import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PaymentTitle from "./PaymentTitle";
import PaymentDetails from "./PaymentDetails";

const DisplayPayments = (props) => {
	const { payments, searchType } = useSelector((state) => state);

	return (
		<React.Fragment>
			{payments.map((payment) => {
				return (
					<div key={payment.id} style={{ width: "50%" }}>
						<PaymentDetails payment={payment} />
					</div>
				);
			})}
		</React.Fragment>
	);
};

export default DisplayPayments;
