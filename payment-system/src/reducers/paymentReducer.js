export const PAYMENT_REDUCER_INITIAL_STATE = {
	payments: [],
	isPaymentsTypeCalled: false,
	apiStatus: "",
	searchType: "",
	isPaymentSaved: false,
	isPaymentAddError: false,
};
const paymentReducer = (state = PAYMENT_REDUCER_INITIAL_STATE, action) => {
	console.log(`Received action ${action.type} in employeeReducer`);
	console.log("action by id", action.payload);
	switch (action.type) {
		case "FETCH_PAYEMNTS_BY_TYPE_SUCCESS":
			return {
				...state,
				payments: action.payload,
				isPaymentsTypeCalled: true,
				searchType: "By Type",
			};

		case "FETCH_PAYEMNTS_API_STATUS_SUCCESS":
			return {
				...state,
				apiStatus: action.payload,
			};
		case "FETCH_PAYEMNTS_BY_ID_SUCCESS":
			return {
				...state,
				payments: action.payload,
				searchType: "By Id",
			};

		case "ADD_PAYMENT_DETAILS_SUCCESS":
			return {
				...state,
				isPaymentSaved: true,
				isPaymentError: false,
			};
		case "ADD_PAYMENT_DETAILS_ERROR":
			return { ...state, isPaymentError: true };

		default: {
			return state;
		}
	}
};

export default paymentReducer;
