import axios from "axios";

const fetchPaymentsByTypeBegin = () => {
	return {
		type: "FETCH_PAYEMNTS_BY_TYPE_BEGIN",
	};
};

const fetchPaymentsByTypeSuccess = (payments) => {
	return {
		type: "FETCH_PAYEMNTS_BY_TYPE_SUCCESS",
		payload: payments,
	};
};

const fetchPaymentsByTypeFailure = (err) => {
	return {
		type: "FETCH_PAYEMNTS_BY_TYPE_FAILURE",
		payload: { message: "Fetching payments by type failed" },
	};
};

const fetchPaymentsByIdBegin = () => {
	return {
		type: "FETCH_PAYEMNTS_BY_ID_BEGIN",
	};
};

const fetchPaymentsByIdSuccess = (payments) => {
	console.log("fetchPaymentsByIdSuccess", payments);
	return {
		type: "FETCH_PAYEMNTS_BY_ID_SUCCESS",
		payload: payments,
	};
};

const fetchPaymentsByIdFailure = (err) => {
	return {
		type: "FETCH_PAYEMNTS_BY_ID_FAILURE",
		payload: { message: "Fetching payments by Id failed" },
	};
};

const fetchPaymentsApiStatusBegin = () => {
	return {
		type: "FETCH_PAYEMNTS_API_STATUS_BEGIN",
	};
};

const fetchPaymentsApiStatusSuccess = (data) => {
	return {
		type: "FETCH_PAYEMNTS_API_STATUS_SUCCESS",
		payload: data,
	};
};

const fetchPaymentsApiStatusFailure = (err) => {
	return {
		type: "FETCH_PAYEMNTS_API_STATUS_FAILURE",
		payload: { message: "Fetching payments by type failed" },
	};
};

const addPaymentDetailsBegin = () => {
	return {
		type: "ADD_PAYMENT_DETAILS_BEGIN",
	};
};

const addhPaymentDetailsSuccess = (success) => {
	return {
		type: "ADD_PAYMENT_DETAILS_SUCCESS",
		payload: success,
	};
};

const addPaymentDetailsError = (error) => {
	return {
		type: "ADD_PAYMENT_DETAILS_ERROR",
		payload: error,
	};
};

export const fetchPaymentsByType = (paytype) => {
	console.log("pay type", paytype);
	return (dispatch, getStatus) => {
		dispatch(fetchPaymentsByTypeBegin);
		const params = { type: paytype.search };
		axios.get(`http://localhost:8080/v1/payment/type/`, { params }).then(
			(res) => {
				dispatch(fetchPaymentsByTypeSuccess(res.data));
			},
			(err) => {
				dispatch(fetchPaymentsByTypeFailure(err));
			}
		);
	};
};
export const fetchPaymentApiStatus = () => {
	return (dispatch, getState) => {
		dispatch(fetchPaymentsApiStatusBegin);
		axios.get(`http://localhost:8080/v1/payment/status`).then(
			(res) => {
				dispatch(fetchPaymentsApiStatusSuccess(res.data));
			},
			(err) => {
				dispatch(fetchPaymentsApiStatusFailure(err));
			}
		);
	};
};

export const fetchPaymentsById = (payId) => {
	return (dispatch, getStatus) => {
		dispatch(fetchPaymentsByIdBegin);
		const params = { id: payId.search };
		axios.get(`http://localhost:8080/v1/payment/find/`, { params }).then(
			(res) => {
				if (res.data instanceof Array) {
					dispatch(fetchPaymentsByIdSuccess(res.data));
				} else {
					dispatch(fetchPaymentsByIdSuccess([res.data]));
				}
			},
			(err) => {
				dispatch(fetchPaymentsByIdFailure(err));
			}
		);
	};
};

const ROOT_URL = "http://localhost:8080/v1/payment/save";

export const addPaymentDetails = (payment) => {
	return (dispatch, getState) => {
		dispatch(addPaymentDetailsBegin());
		console.log("state after addPaymentDetailsBegin", getState());
		axios.post(ROOT_URL, payment).then(
			(res) => {
				console.log(res);
				dispatch(addhPaymentDetailsSuccess(true));
				console.log("state after addPaymentDetailsSuccess", getState());
			},
			(err) => {
				console.log(err);
				dispatch(addPaymentDetailsError(true));
				console.log("state after addPaymentDetailsError", getState());
			}
		);
	};
};
